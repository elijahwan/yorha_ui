import {h, render} from "vue";

export default {
    // 渐入动画
    fadeIn(component, props) {
        let container = document.createElement('div')
        const VNode = h(component, props)
        render(VNode, container)
        document.body.appendChild(container)

        return container
    },

    // 渐出动画
    fadeOut(container, timeOut, callBack) {
        container.firstElementChild.style.opacity = 0
        setTimeout(()=>{
            if (callBack) callBack()
            document.body.removeChild(container)
        },timeOut)
    }
}