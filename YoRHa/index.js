
import YorhaSmallButton from "./components/YorhaSmallButton/index.js";
import YorhaButton from "./components/YorhaButton/index.js";
import YorhaButtonGroup from "./components/YorhaButtonGroup/index.js";
import YorhaWarningBox from "./components/YorhaWarningBox";
import YorhaHr from "./components/YorhaHr/index.js";
import YorhaNormalBox from "./components/YorhaNormalBox/index.js";
import YorhaBigTitle from "./components/YorhaBigTitle/index.js";
import YorhaTab from "./components/YorhaTab/index.js";
import YorhaCheckBox from "./components/YorhaCheckBox/index.js";
import YorhaRadio from "./components/YorhaRadio/index.js";
import YorhaImageBox from "./components/YorhaImageBox/index.js";
import YorhaInput from "./components/YorhaInput/index.js";
import YorhaSwitch from "./components/YorhaSwitch/index.js";
import YorhaSelect from "./components/YorhaSelect/index.js";

import YorhaMessageBox from "./components/YorhaMessageBox/index.js";
import YorhaDialog from "./components/YorhaDialog/index.js";
import YorhaBigDialog from "./components/YorhaBigDialog/index.js";
import YorhaBigLoading from "./components/YorhaBigLoading/index.js";
import YorhaSmallLoading from "./components/YorhaSmallLoading/index.js";
import YorhaTabButton from "./components/YorhaTabButton/index.js";
import YorhaListBox from "./components/YorhaListBox/index.js";
import YorhaTree from "./components/YorhaTree/index.js";

const components = [

    YorhaSmallButton,
    YorhaButton,
    YorhaHr,
    YorhaTab,
    YorhaSwitch,
    YorhaCheckBox,
    YorhaRadio,
    YorhaButtonGroup,
    YorhaNormalBox,
    YorhaWarningBox,
    YorhaBigTitle,
    YorhaTabButton,
    YorhaSelect,
    YorhaImageBox,
    YorhaInput,
    YorhaListBox,
    YorhaTree
]

const install = (Vue) => {
    // 判断是否安装
    if (install.installed) return
    // 遍历注册全局组件
    components.map(component => Vue.component(component.name, component))
}

if (typeof window !== 'undefined' && window.Vue) {
    install(window.Vue);
}

export default {
    install,
    YorhaSmallButton,
    YorhaButton,
    YorhaHr,
    YorhaTab,
    YorhaSwitch,
    YorhaButtonGroup,
    YorhaWarningBox,
    YorhaNormalBox,
    YorhaBigTitle,
    YorhaTabButton,
    YorhaCheckBox,
    YorhaRadio,
    YorhaImageBox,
    YorhaSelect,
    YorhaListBox,
    YorhaTree
}

export {
    YorhaMessageBox,
    YorhaDialog,
    YorhaBigDialog,
    YorhaSmallLoading,
    YorhaBigLoading,
    YorhaInput
}