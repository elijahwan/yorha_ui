import YorhaTree from "./YorhaTree.vue";

YorhaTree.install = Vue => {
    Vue.component(YorhaTree.name, YorhaTree)
}

export default YorhaTree