import YorhaSmallLoadingComponent from "./YorhaSmallLoading.vue";
import transition from "../../utils/transition.js";

export default class YorhaSmallLoading {
    container

    constructor(contain) {
        const props = {contain}
        this.container = transition.fadeIn(YorhaSmallLoadingComponent, props)
        document.body.style.overflowY = 'hidden'
    }

    close() {
        transition.fadeOut(this.container, 300)
        document.body.style.overflowY = ''
    }
}

