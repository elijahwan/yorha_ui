import YorhaBigTitle from "./YorhaBigTitle.vue";

YorhaBigTitle.install = Vue =>{
    Vue.component(YorhaBigTitle.name, YorhaBigTitle)
}

export default YorhaBigTitle