import YorhaBigLoadingCom from "./YorhaBigLoading.vue";
import transition from "../../utils/transition.js";

export default class YorhaBigLoading {
    container
    msg
    autoClose
    constructor(msg, autoClose) {
        this.msg = msg
        this.autoClose = autoClose
    }

    open() {
        return new Promise((resolve, reject) => {
            let props = {
                msg:this.msg,
                closeFn:()=>{
                    if (this.autoClose)  transition.fadeOut(this.container, 300)
                    else resolve()
                }
            }
            this.container = transition.fadeIn(YorhaBigLoadingCom, props)
            document.body.style.overflowY = 'hidden'
        })
    }

    close() {
        transition.fadeOut(this.container, 300)
        document.body.style.overflowY = ''
    }
}