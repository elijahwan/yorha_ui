import YorhaSmallButton from './YorhaSmallButton.vue'

YorhaSmallButton.install = (Vue)=>{
    Vue.component(YorhaSmallButton.name,YorhaSmallButton)
}

export default YorhaSmallButton