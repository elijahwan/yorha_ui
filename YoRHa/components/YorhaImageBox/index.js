import YorhaImageBox from "./YorhaImageBox.vue";

YorhaImageBox.install = Vue =>{
    Vue.component(YorhaImageBox.name, YorhaImageBox)
}

export default YorhaImageBox