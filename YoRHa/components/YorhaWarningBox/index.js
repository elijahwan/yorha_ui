import YorhaWarningBox from "./YorhaWarningBox.vue";

YorhaWarningBox.install = Vue =>{
    Vue.component(YorhaWarningBox.name, YorhaWarningBox)
}

export default YorhaWarningBox
