import YorhaRadio from "./YorhaRadio.vue";

YorhaRadio.install = Vue => {
    Vue.component(YorhaRadio.name, YorhaRadio)
}

export default YorhaRadio