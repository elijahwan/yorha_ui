import transition from "../../utils/transition.js";
import YorhaDialog from "./YorhaDialog.vue"

export default {
    ask(title, contain) {
        return new Promise((resolve, reject) => {
            let container
            const props = {
                title,
                contain,
                cancelHandler: ()=>{
                    transition.fadeOut(container, 300, reject())
                    document.body.style.overflowY = ''
                },
                confirmHandler: ()=>{
                    transition.fadeOut(container, 300, resolve())
                    document.body.style.overflowY = ''
                }
            }
            container = transition.fadeIn(YorhaDialog, props)
            document.body.style.overflowY = 'hidden'
        })
    }
}