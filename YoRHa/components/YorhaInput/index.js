import YorhaInput from "./YorhaInput.vue";

YorhaInput.install = Vue => {
    Vue.component(YorhaInput.name, YorhaInput)
}

export default YorhaInput