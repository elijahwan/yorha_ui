import YorhaBigDialog from './YorhaBigDialog.vue'
import transition from "../../utils/transition.js";

export default {

    ask(speaker, steps, needSelect, selectBtns) {

        return new Promise((resolve, reject) => {
            let container
            const props = {
                speaker,
                steps,
                needSelect,
                selectBtns,
                selectAnswer:value=>{
                    transition.fadeOut(container, 300, resolve(value))
                    document.body.style.overflowY = ''
                }
            }
            container = transition.fadeIn(YorhaBigDialog, props)
            document.body.style.overflowY = 'hidden'
        })
    }
}