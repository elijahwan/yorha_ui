import YorhaMessageBox from '../YorhaMessageBox/YorhaMessageBox.vue'
import transition from "../../utils/transition.js";

export default {
    confirm(title, contain) {
        return new Promise(resolve => {
            let container
            const props = {
                title,
                contain,
                confirmHandler:()=>{
                    transition.fadeOut(container, 300, resolve())
                    document.body.style.overflowY = ''
                }
            }
            container = transition.fadeIn(YorhaMessageBox, props)
            document.body.style.overflowY = 'hidden'
        })
    }
}