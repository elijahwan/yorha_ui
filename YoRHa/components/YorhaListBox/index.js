import YorhaListBox from "./YorhaListBox.vue";

YorhaListBox.install = Vue => {
    Vue.component(YorhaListBox.name, YorhaListBox)
}

export default YorhaListBox