# 尼尔机械纪元UI组件库

```angular2html
!!!注意:本组件库仅适用Vue3项目，并不适用Vue2!!!
```

项目演示地址http://114.116.47.23:3000/#/login  
UserName:2B  
Password:AllToBeNice

## 安装

请在终端中输入

```
npm i nier-yorha-ui
```

main.js

```ecmascript 6
import YoRHa from 'nier-yorha-ui'
import 'nier-yorha-ui/style.css'

const app = createApp(App)
app.use(YoRHa)
```

## 使用

### 按钮

#### 小按钮

```angular2html

<yorha-small-button @click="demo">Demo</yorha-small-button>
```

#### 普通按钮

```angular2html

<yorha-button @click="demo">Demo</yorha-button>
```

#### 按钮组

```angular2html

<yorha-button-group type="checkBox" :buttons="btns" @click="demo"></yorha-button-group>
```

参数  
type：checkBox，radio；可以将按钮组设置为checkBox或者radio  
buttons：传入的按钮列表对象，请使用以下格式

```ecmascript 6
btns = {
    'Type one': true, // 表示初始被选定
    'Type two': false, // 表示初始不被选定
    'Type three': false,
}
```

响应事件  
v-on:click  
若type设置为checkBox，对于函数的第一个参数为选定的数组。  
若type设置为radio，对于函数的第一个参数为btns中对应的键值

#### 顶栏按钮与按钮组

顶栏按钮  
并不推荐单独使用

```angular2html

<yorha-tab-button :is-active="isActive" @click="demo">Demo</yorha-tab-button>
```

参数  
is-active：表示按钮是否被选中 顶栏按钮组  
推荐与路由结合使用实现切换效果

```angular2html

<yorha-tab>
    <router-link v-for="(path, name) in tabs"
                 :to="path"
                 custom
                 v-slot="{isActive, navigate}">
        <yorha-tab-button :is-active="isActive" @click="navigate">{{name}}</yorha-tab-button>
    </router-link>
</yorha-tab>
```

### 分割栏

```angular2html
<yorha-hr></yorha-hr>
```

### 数据显示

#### 树形控件

```angular2html
<yorha-tree :tree-data="treeData" :auto-read="true" @select="selectHandler"></yorha-tree>
```

```ecmascript 6
let treeData = ref([
    {
        title: '完成项目需求分析',
        id: '1',
        tag: true,
        children: [
            {title: '调研需求', id: '11', tag: false},
            {title: '编写文档', id: '12', tag: true},
        ]
    },
    {
        title: '完成项目搭建',
        id: '2',
        tag: true,
        children: [
            {title: '前端搭建', id: '21', tag: true},
            {
                title: '后端搭建', id: '22', tag: true, children: [
                    {title: '项目测试', id: '221', tag: true}
                ]
            },
        ]
    },
    {
        title: '项目上线',
        id: '3',
        tag: true,
    },
])

function selectHandler(id) {
    console.log(treeData.value)
}
```
参数：
treeData：用于显示的树形控件数据，请务必使用多维数组格式
:auto-read：为true时，当单击某个列表项，其tag属性将自动完成，此时请使用v-model:tree-data
@select：单击某个列表项时刻的响应事件
### 容器盒子

```
注意：容器盒子都是块元素
```

#### 图片盒子

```angular2html

<yorha-image-box width="100px" height="100px">
    <img src="http://114.116.47.23:8848/6O.jpg" width="93" height="93">
</yorha-image-box>
```

参数  
width与height为整个容器的尺寸，内部子元素的尺寸为width&height-3px

#### 普通盒子

```angular2html

<yorha-normal-box width="550px" height="300px" :title="title">
    <div>demo</div>
</yorha-normal-box>
```

参数  
width与height为整个容器的尺寸  
title：盒子容器的标题

#### 列表盒子

```ecmascript 6
<yorha-list-box></yorha-list-box>
```

其本质与普通盒子没有区别，知识较适合与树形列表一同配合使用

### 弹出层组件

#### 小加载

```ecmascript 6
import {YorhaSmallLoading} from "nier-yorha-ui"; // 引入

let loading = new YorhaSmallLoading('Please wait...') // 打开
loading.close() // 关闭
```

#### 大加载

```ecmascript 6
import {YorhaBigLoading} from "nier-yorha-ui";

let msg = [['demo', 'one'], ['demo2', 'two']]
let l = new YorhaBigLoading(msg, false) 
```

参数  
msg传入并显示的信息，要求格式为双层数组  
false表示自动关闭取消，若为true，该组件将在信息显示完毕一秒后关闭  
若为false则需要手动关闭

```ecmascript 6
l.open() // 打开

setTimeout(() => {
    l.close() // 关闭
}, 3000)
```

#### 对话框

```ecmascript 6
import {YorhaDialog} from "nier-yorha-ui";

// ask(对话框标题, 对话框内容)
YorhaDialog.ask('DANGER', question).then(() => {
    console.log('confirm') // 点击确定的处理逻辑
}).catch(() => {
    console.log('cancel') // 点击取消的处理逻辑
})
```

#### 大对话框

```ecmascript 6
import {YorhaDialog} from "nier-yorha-ui";

// 每一步显示的内容
let steps = [
    'If you want to play this game',
    'You need to select your gamer type first',
    'Now select'
]

// 最终需要选择的按钮
let btns = {
    'Type one': true,
    'Type two': false,
    'Type three': false,
}

// answer表示选择的按钮的值
YorhaBigDialog.ask('POD042', steps, true, btns).then(answer => {
    console.log(answer) // 点击某个按钮之后的处理逻辑
})
```





