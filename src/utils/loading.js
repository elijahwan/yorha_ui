import YorhaLoading from "../components/YorhaLoading/YorhaLoading.vue";
import {createApp} from "vue";

let loading = {}
loading.install = function (app) {
    const loading = createApp(YorhaLoading)
    const instance = loading.mount(document.createElement('div'))
    document.body.appendChild(instance.$el)
}



export default loading