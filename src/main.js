import {createApp} from 'vue'
import '../YoRHa/style.css'
import App from './App.vue'
import YoRHa from "../YoRHa/index.js";
// import YoRHa from 'nier-yorha-ui'
// import 'nier-yorha-ui/style.css'

const app = createApp(App)
app.use(YoRHa)
app.mount('#app')
