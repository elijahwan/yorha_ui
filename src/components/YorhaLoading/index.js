import YorhaLoadingComponent from "./YorhaLoading.vue";
import transition from "../../utils/transition.js";

export default class YorhaLoading {
    container

    constructor(contain) {
        const props = {contain}
        this.container = transition.fadeIn(YorhaLoadingComponent, props)
    }

    close() {
        transition.fadeOut(this.container, 300)
    }
}

