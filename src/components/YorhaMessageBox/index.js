import YorhaMessageBox from './YorhaMessageBox.vue'
import transition from "../../utils/transition.js";

export default {
    confirm(title, contain) {
        return new Promise(resolve => {
            let container
            const props = {
                title,
                contain,
                confirmHandler:()=>{
                    transition.fadeOut(container, 300, resolve())
                }
            }
            container = transition.fadeIn(YorhaMessageBox, props)
        })
    }
}